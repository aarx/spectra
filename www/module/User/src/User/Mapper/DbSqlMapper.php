<?php
 namespace User\Mapper;

 use User\Model\UserInterface;

 use Zend\Db\Adapter\AdapterInterface;
 use Zend\Db\Adapter\Driver\ResultInterface;
 use Zend\Db\ResultSet\HydratingResultSet;
 use Zend\Db\Sql\Delete;
 use Zend\Db\Sql\Insert;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Update;
 use Zend\Stdlib\Hydrator\HydratorInterface;


 class DbSqlMapper implements UserMapperInterface
 {
     /**
      * @var \Zend\Db\Adapter\AdapterInterface
      */
     protected $dbAdapter;

     /**
      * @var \Zend\Stdlib\Hydrator\HydratorInterface
      */
     protected $hydrator;

     /**
      * @var \User\Model\PostInterface
      */
     protected $userPrototype;

     /**
      * @param AdapterInterface  $dbAdapter
      * @param HydratorInterface $hydrator
      * @param UserInterface    $userPrototype
      */
     public function __construct(
         AdapterInterface $dbAdapter,
         HydratorInterface $hydrator,
         UserInterface $userPrototype
     ) {
         $this->dbAdapter      = $dbAdapter;
         $this->hydrator       = $hydrator;
         $this->userPrototype  = $userPrototype;
     }

     /**
      * @param int|string $id
      *
      * @return UserInterface || boolean false
      */
     public function find($steamId)
     {
         $sql    = new Sql($this->dbAdapter);
         $select = $sql->select('user');
         $select->where(array('steam_id = ?' => $steamId));

         $stmt   = $sql->prepareStatementForSqlObject($select);
         $result = $stmt->execute();

         if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
            //die(print_r($result->current(), true));
            $arrUser = array();
            $arrUser['id'] = $result->current()['user_id'];
            $arrUser['steamId'] = $result->current()['steam_id'];
            $arrUser['personaName'] = $result->current()['persona_name'];
            $arrUser['avatar'] = $result->current()['avatar'];
            $arrUser['hasGame'] = $result->current()['has_csgo'];
            return $this->hydrator->hydrate($arrUser, $this->userPrototype);
          }

         return false;
     }

     /**
      * @return array|UserInterface[]
      */
     public function findAll()
     {
         $sql    = new Sql($this->dbAdapter);
         $select = $sql->select('user');

         $stmt   = $sql->prepareStatementForSqlObject($select);
         $result = $stmt->execute();

         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);

             return $resultSet->initialize($result);
         }

         return array();
     }

     /**
      * {@inheritDoc}
      */
     public function save(UserInterface $userObject)
     {
         $userData = $this->hydrator->extract($userObject);
         unset($userData['id']); // Neither Insert nor Update needs the ID in the array
         $userData['steam_id'] = $userData['steamId'];
         $userData['persona_name'] = $userData['personaName'];
         $userData['has_csgo'] = $userData['hasGame'];
         unset($userData['steamId']);
         unset($userData['personaName']);
         unset($userData['hasGame']);
         //die(print_r($userData, true));
         if ($userObject->getId()) {
             // ID present, it's an Update
             $action = new Update('user');
             $action->set($userData);
             $action->where(array('user_id = ?' => $userObject->getId()));
         } else {
             // ID NOT present, it's an Insert
             $action = new Insert('user');
             $action->values($userData);
         }

         $sql    = new Sql($this->dbAdapter);
         $stmt   = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();

         if ($result instanceof ResultInterface) {
             return $userObject;
         }

         throw new \Exception("Database error");
     }

     /**
      * {@inheritDoc}
      */
     public function delete(UserInterface $userObject)
     {
         $action = new Delete('user');
         $action->where(array('id = ?' => $postObject->getId()));

         $sql    = new Sql($this->dbAdapter);
         $stmt   = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();

         return (bool)$result->getAffectedRows();
     }
 }