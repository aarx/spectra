<?php

 namespace User\Model;

 class User implements UserInterface
 {
     /**
      * @var int
      */
     protected $id;

     /**
      * @var int
      */
     protected $steamId;

     /**
      * @var string
      */
     protected $personaName;

     /**
      * @var string
      */
     protected $avatar;

     /**
      * @var tinyint
      */
     protected $hasGame;

     /**
      * {@inheritDoc}
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param int $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * {@inheritDoc}
      */
     public function getSteamId()
     {
         return $this->steamId;
     }

      /**
      * @param int $steamId
      */
     public function setSteamId($steamId)
     {
         $this->steamId = $steamId;
     }

     /**
      * {@inheritDoc}
      */
     public function getPersonaName()
     {
         return $this->personaName;
     }

     /**
      * @param string $personaName
      */
     public function setPersonaName($personaName)
     {
         $this->personaName = $personaName;
     }

     /**
      * {@inheritDoc}
      */
     public function getAvatar()
     {
         return $this->avatar;
     }

     /**
      * @param string $avatar
      */
     public function setAvatar($avatar)
     {
         $this->avatar = $avatar;
     }

     /**
      * {@inheritDoc}
      */
     public function getHasGame()
     {
         return $this->hasGame;
     }

     /**
      * @param tinyint $hasGame
      */
     public function setHasGame($hasGame)
     {
         $this->hasGame = $hasGame;
     }
 }