<?php

 namespace User\Model;

 interface UserInterface
 {
     /**
      * Will return the ID of the user
      *
      * @return int
      */
     public function getId();

     /**
      * Will return the steamID of the user
      *
      * @return int
      */
     public function getSteamId();

     /**
      * Will return the personaName of the user
      *
      * @return string
      */
     public function getPersonaName();

     /**
      * Will return the AVATAR of the steamUser
      *
      * @return string
      */
     public function getAvatar();

     /**
      * Will return the hasGame of the steamUser
      *
      * @return int
      */
     public function getHasGame();
 }