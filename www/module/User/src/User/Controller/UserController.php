<?php

namespace User\Controller;

use User\Service\UserServiceInterface;
use User\Model\User;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class UserController extends AbstractActionController
{
    protected $testTable;
    private $_STEAMAPI = 'D0D704AD24C14D91F39DCDA801375235';

    /**
      * @var \User\Service\UserServiceInterface
      */
     protected $userService;

    /**
      * Session variable
      * @var \User\Controller\IndexController
      */
     protected $user;

    public function __construct(
        UserServiceInterface $userService)
    {
        $this->userService = $userService;
        $this->user = new Container('user');
    }

    public function indexAction()
    {
        return new ViewModel(array(
                            'user' => $this->user));
    }

    public function loginAction()
    {
        include('LightOpenID.php');

        try {
            $openID = new LightOpenID('http://spectra.ambassador.ee/www/public/');

            if(!$openID->mode) {
                $openID->identity = 'http://steamcommunity.com/openid/?l=english';
                $this->redirect()->toUrl($openID->authUrl());

            } elseif($openID->mode == 'cancel') {
                echo 'User has cancelled login';

            } else {
                if($openID->validate()) {
                    $id = $openID->identity;
                    $ptn = 'http://steamcommunity.com/openid/id/';
                    $steamid = str_replace($ptn, '', $id);


                    $url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . $this->_STEAMAPI . '&steamids=' . $steamid . '&foramt=json';
                    $json_object = file_get_contents($url);

                    $json_decoded = json_decode($json_object);

                    foreach($json_decoded->response->players as $player) {

                        $userData = new User();

                        $url = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=' . $this->_STEAMAPI . '&steamid=' . $steamid . '&format=json';
                        $json_object = file_get_contents($url);
                        $json_decoded = json_decode($json_object);
                        foreach($json_decoded->response->games as $game) {
                            if($game->appid == 730) {
                                $userData->setHasGame(1);
                                break;
                            } else {
                                $userData->setHasGame(0);
                            }
                        }
                        $userData->setSteamId($player->steamid);
                        $userData->setPersonaName($player->personaname);
                        $userData->setAvatar($player->avatarmedium);


                        if($user = $this->userService->findUser($userData->getSteamId())) {
                            $userData->setId($user->getId());
                            //die(print_r($user, true));
                        }
                        if($this->userService->saveUser($userData)) {
                            $this->user->username = $player->personaname;
                            $this->user->steamid = $player->steamid;
                            $this->user->userlink = $player->profileurl;
                            $this->user->avatar = $player->avatarfull;
                            $this->user->hasGame = $userData->getHasGame();

                        }


                    }
                    $this->redirect()->toRoute('home',
                                                array('user' => $this->user
                                                ));
                } else {
                    echo 'User is not logged in';
                }
            }

        } catch(ErrorException $e) {
            echo $e->getMessage();
        }
    }

    public function logoutAction()
    {
        if($this->user) {
            $this->user->getManager()->destroy();
            return $this->redirect()->toRoute('home');
        }
    }

}
