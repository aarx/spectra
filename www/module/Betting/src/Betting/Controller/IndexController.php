<?php

namespace Betting\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use LightOpenId\LightOpenID;

class IndexController extends AbstractActionController
{
    protected $testTable;
    private $_STEAMAPI = 'D0D704AD24C14D91F39DCDA801375235';

    /**
      * Session variable
      * @var \User\Controller\IndexController
      */
     protected $user;

    public function __construct()
    {
         $this->user = new Container('user');
    }

    public function indexAction()
    {

        return new ViewModel(array(
                            'user' => $this->user,
                            'session' => print_r($_SESSION, true)));
    }


}
