<?php

 namespace Chat\Controller;

 use Chat\Service\MessageServiceInterface;
 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\Form\FormInterface;

 class ListController extends AbstractActionController
 {
     /**
      * @var \Chat\Service\ChatServiceInterface
      */
     protected $messageService;

     protected $messageForm;
     
     public function __construct(
        MessageServiceInterface $messageService,
        FormInterface $messageForm)
     {
         $this->messageService = $messageService;
         $this->messageForm    = $messageForm;
     }
     
     public function indexAction()
     {
        $request = $this->getRequest();

         if ($request->isPost()) {
             $this->messageForm->setData($request->getPost());

             if ($this->messageForm->isValid()) {
                 try {
                     //\Zend\Debug\Debug::dump($this->messageForm->getData());die();
                     $this->messageService->saveMessage($this->messageForm->getData());
                     
                 } catch (\Exception $e) {
                     echo $e->getMessage();
                     // Some DB Error happened, log it and let the user know
                 }
             }
         }
         
        return new ViewModel(array(
            'messagesInfo' => array(
                'conversationName' => $this->messageService->
                'messages' => $this->messageService->findAllMessages(),
            ),
            'form' => $this->messageForm
        ));
     }
     
     public function detailAction()
     { 
         $id = $this->params()->fromRoute('id');

        try {
             $message = $this->messageService->findMessage($id);
         } catch (\InvalidArgumentException $ex) {
             return $this->redirect()->toRoute('messages');
         }

         return new ViewModel(array(
             'message' => $message
         ));
     }
 }

?>