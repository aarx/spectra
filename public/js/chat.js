
$(document).ready(function(){
    $('.chatFeed').scrollTop($('.chatFeed')[0].scrollHeight);
    
    var power = true;
    
        console.log(power);
        
        var fiveMinutes = 60 * 5,
            display = $('.timer');
        
      
        $(".textField").focus(
             
            function(){
            startTimer(fiveMinutes, display);
            
            $('.textField').off();
        });
    
     
    
    
    function startTimer(duration, display) {
    var timer = duration, minutes, seconds;  
        
        setInterval(countDown, 1000); 
        
    function countDown() {
        
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + " : " + seconds);
      
        --timer;
        
        if (timer < 0) {
            timer = 0;
 
        }
    }
}
        
    
       
     
    
    
   
    
    
    
    $('form').submit(function(event) {
        event.preventDefault();
        var formData = $(this).serialize();
        if($('input[name="message-fieldset[message]"]').val()) {
            var request = $.ajax({
                type : 'POST',
                url : $(this).attr('action'),
                data : formData
            }),
             chained = request.pipe(function(data) {
                return $.ajax({
                    type : 'POST',
                    url : '/messages',
                    data : {mode : 'json', user2 : 13},
                    dataType : 'json'
                });
            });
            chained.done(function(data) {
                //console.log(JSON.stringify(data.messages.message, null, 2));
                $('input[name="message-fieldset[message]"]').val('');
                $('.chatFeed:last').fadeIn('slow', function() {
                    if(data.messages.userId == data.loggedUser.userId) {
                        var messageDivClass = 'reporter';
                    } else {
                        var messageDivClass = 'interviewee';
                    }
                    $('.chatFeed').append('<div class="message-container"><div class="'+messageDivClass+' box"><div class="line"><div class="timestamp-container"><span class="timestamp">'+data.messages.timestamp+'</span></div><strong>'+data.messages.username+' </strong><span class="user-functions"><a href="/messages/edit/376">Edit</a><a href="/messages/delete/376"> Delete</a></span></div><div><span class="message'+data.messages.id+'"><p>'+data.messages.message+'</p></span></div></div><br class="clear"></div>');
                });
                $('.chatFeed').scrollTop($('.chatFeed')[0].scrollHeight);
                callServer(data);
                
            });
        }
        
    });
    
    
});
