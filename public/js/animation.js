

var safeColors = ['00','33','66','99','cc','ff'];
var rand = function() {
    return Math.floor(Math.random()*6);
};
var randomColor = function() {
    var rhex = safeColors[rand()];
    var ghex = safeColors[rand()];
    var bhex = safeColors[rand()];
    var hex = "#"+rhex+ghex+bhex;
    hex = hex.replace('#','');
    var r = parseInt(hex.substring(0,2), 16);
    var g = parseInt(hex.substring(2,4), 16);
    var b = parseInt(hex.substring(4,6), 16);
    var result = 'rgba('+r+','+g+','+b+','+.1+')';
    return result;
};





$(document).ready(function() {
        $('.friend-list-friends').each(function() {
            $(this).css('background',randomColor());
       
    });
});
