/*$.ajax({
    type : 'POST',
    url : '/user/tmp',
    data : {getUser : true},
})
.done(function(data) {
    var username = data.username;
    //console.log(username+'  '+window.location.href+' and '+(window.location.href).includes(username));
    if(!(window.location.href).includes(username)) {
        $('.user-info').removeClass('editable');
    }
});
*/


$('.confirm').on('click', function(e) {
    var key = e.which;
    //if(e.type == 'click' || key == '13') {
        if($(this).prev('.birthday-date-select').length) {
            var year = $('#year').val();
            var month = parseInt($('#month').val()) + 1;
            var day = $('#day').val();
            var value = year+'-'+month+'-'+day;
        } else if($(this).prev('.name').length) {
            var first = $('#txtbox-first').val();
            var middle = $('#txtbox-middle').val();
            var last = $('#txtbox-last').val();
            var data = 'firstName='+first +'&middleName=' +middle+ '&lastName=' +last;
        } else {
            var value = $(this).prev().val().trim();
        }

        $(this).hide();
        $('.input').hide();
        $(this).prev('.birthday-date-select').hide();
        $(this).next().show();
        var parameterName = $(this).attr('name').trim();

        if(!data) {
            var data = parameterName +'='+ value;
        }

        $.ajax({
            type : 'POST',
            url : '/edit',
            data : data,
        })
        .done(function(data) {
            if(typeof data.result != 'undefined') {
                $(this).next().text(data.result.userData[1]);
            console.log(data.result.userData[0]);
            console.log(JSON.stringify(data, null, '\t'));
            }
        });
        $(this).parent().addClass('editable');
    //}
    
});

$('.editable').on('click', function() {
    $(this).removeClass('editable');
    $(this).children('.txtbox-value').hide();
    $(this).children('.input').show();
    $(this).children('.confirm').show();
    
    if($(this).children('.birthday-date-select').length) {
        var date = new Date($(this).children('.txtbox-value').attr('raw'));
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        $('.birthday-date-select > #day').val(day);
        $('.birthday-date-select > #month').val(month);
        $('.birthday-date-select > #year').val(year);
        $('.birthday-date-select').show();
    } else if($(this).children('.name').length) {
        var first = $('#txtbox-first').val();
        var middle = $('#txtbox-middle').val();
        var last = $('#txtbox-last').val();
        $('.name > #txtbox-first').val(first);
        $('.name > #txtbox-middle').val(middle);
        $('.name > #txtbox-last').val(last);
        $('.name').show();
    }
    $(this).off('click');
    
    
});


