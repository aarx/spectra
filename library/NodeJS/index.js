/*var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
io.set('transports',['xhr-polling']);
var os = require("os");

server.listen(8000);
io.sockets.on('connection', function (socket) {
    // when the client emits 'loadsession'
    console.log('user connected');
    socket.on('loadsession', function () {
        // we tell the client to execute 'updatechat' with empty parameter
        io.sockets.emit('initNodes', "");
    });    
    socket.on('chat', function (data) {
        // we tell the client to execute 'updateNodes' with 1 parameter
        io.sockets.emit('updateNodes', data);
        console.log(data);
    });
});
server.listen('', function(){
console.log("Server up and running on "+os.hostname()+"....");
});*/
/*
var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
io.on('connection', function(socket){
    socket.on('connected', function(data) {
        console.log(data);
    });
});
server.listen(3000);
server.listen('', function(){
    var os = require("os");
    console.log("Server up and running on "+os.hostname()+"....");
});*/
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});