<?php

 return array(
     'service_manager' => array(
        'factories' => array(
            'User\Mapper\UserMapperInterface' => 'User\Factory\ZendDbSqlMapperFactory',
            'User\Service\UserServiceInterface' => 'User\Factory\UserServiceFactory',
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'User\Mapper\EditMapperInterface' => 'User\Factory\EditMapperFactory',
            'User\Service\EditServiceInterface' => 'User\Factory\EditServiceFactory',
        )
     ),
     
    'controllers' => array(
        'factories' => array(
            'User\Controller\User' => 'User\Factory\UserControllerFactory',
            'User\Controller\Edit' => 'User\Factory\EditControllerFactory',
        ),
    ),
     
     'router' => array(
         'routes' => array(
             'user' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/user/:user[/:action]',
                     'constraints' => array(
                         'user' => '[a-zA-Z][a-zA-Z0-9_-]*',
                     ),
                     'defaults' => array(
                         'controller' => 'User\Controller\User',
                         'action'     => 'index',
                     ),
                 ),
             ),
             'register' => array(
                 'type'    => 'literal',
                 'options' => array(
                     'route'    => '/register',
                     'defaults' => array(
                         'controller' => 'User\Controller\User',
                         'action'     => 'register',
                     ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'success' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/success',
                            'defaults' => array(
                                'controller' => 'User\Controller\User',
                                'action' => 'success'
                            )
                        )
                    ),
                ),
            ),
            'login' => array(
                 'type'    => 'literal',
                 'options' => array(
                     'route'    => '/login',
                     'defaults' => array(
                         'controller' => 'User\Controller\User',
                         'action'     => 'login',
                     ),
                ), 
            ),
            'edit' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/edit[/:action]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                     ),
                     'defaults' => array(
                         'controller' => 'User\Controller\Edit',
                         'action'     => 'index',
                     ),
                ), 
            ),
            'upload' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/upload[/:action]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                     ),
                     'defaults' => array(
                         'controller' => 'User\Controller\Edit',
                         'action'     => 'uploadForm',
                     ),
                ), 
            ),
         )
     ),
     
     'view_manager' => array(
        'exception_template' => 'error/index',
            'template_map' => array(
                'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
     ),
 );

?>