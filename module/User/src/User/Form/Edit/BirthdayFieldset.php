<?php

namespace User\Form\Edit;

use User\Model\Edit;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

class BirthdayFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new Edit());

      $date = array();
      for($i=1;$i<32;$i++) {
         $date[$i] = $i;
        
      }
      $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'day',
             'options' => array(
                     'empty_option' => 'Day',
                     'value_options' => $date
             )
     ));
     $date = array();
     for($i=0;$i<12;$i++) {
         $date[$i] = $i+1;
        
     }
     $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'month',
             'options' => array(
                     'empty_option' => 'Month',
                     'value_options' => $date
             )
     ));
    
     $date = array();
     for($i=intval(date('Y'));$i>1950;$i--) {
         $date[$i] = $i;
        
     }
     $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'year',
             'options' => array(
                     'empty_option' => 'Year',
                     'value_options' => $date
             )
     ));

   }
}

?>