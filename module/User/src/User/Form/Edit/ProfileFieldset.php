<?php

namespace User\Form\Edit;

use User\Model\Edit;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProfileFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new Edit());

      $this->add(array(
         'type' => 'text',
         'name' => 'username',
         'attributes'=>array(
            'size'=> '100',
            'required' => true,
             'class' => 'login-form userName',
             'placeholder' => 'Insert your username',
             'autocomplete' => 'off',
             
        )
      ));

      $this->add(array(
         'type' => 'password',
         'name' => 'password',
         'attributes'=>array(
            'size'=> '100',
            'required' => true,
             'class' => 'login-form password',
             'placeholder' => 'Insert your password',
             'autocomplete' => 'off',
        )
      ));
   }
}

?>