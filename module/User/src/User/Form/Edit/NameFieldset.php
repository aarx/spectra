<?php

 namespace User\Form\Edit;

 use User\Model\Edit;
 use Zend\Form\Fieldset;
 use Zend\Stdlib\Hydrator\ClassMethods;

class NameFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new Edit());

      $this->add(array(
         'type' => 'text',
         'name' => 'firstName',
         'attributes'=>array(
            'size'=> '100',
             'class' => 'input',
             'autocomplete' => 'off',
             
            )
      ));
       
       $this->add(array(
         'type' => 'text',
         'name' => 'middleName',
         'attributes'=>array(
            'size'=> '100',
             'class' => 'input',
             'placeholder' => 'Your middle name',
             'autocomplete' => 'off',
             
            )
      ));
       
       $this->add(array(
         'type' => 'text',
         'name' => 'lastName',
         'attributes'=>array(
            'size'=> '100',
             'class' => 'input',
             'autocomplete' => 'off',
            )
      ));
   }
}

?>