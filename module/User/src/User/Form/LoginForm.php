<?php

 namespace User\Form;

 use Zend\Form\Form;

 class LoginForm extends Form
 {
     public function __construct($name = null, $options=array())
     {
         
         parent::__construct($name, $options);
         
         $this->add(array(
             'name' => 'login-fieldset',
             'type' => 'User\Form\LoginFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));
         
         $this->add(array(
             'type' => 'Zend\Form\Element\Csrf',
             'name' => 'csrf',
         ));

         $this->add(array(
             'type' => 'submit',
             'name' => 'submit',
             'attributes' => array(
                 'value' => 'Login',
                 'class' => 'user-input button-user'
             )
         ));
     }
 }

?>