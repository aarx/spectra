<?php

 namespace User\Form;
 
 use Zend\InputFilter;
 use Zend\Form\Element;
 use Zend\Form\Form;

 class EditForm extends Form
 {
     public function __construct($name = null, $options=array())
     {
         
         parent::__construct($name, $options);
         
         $this->add(array(
             'name' => 'name-fieldset',
             'type' => 'User\Form\Edit\NameFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));
         
         $this->add(array(
             'name' => 'birthday-fieldset',
             'type' => 'User\Form\Edit\BirthdayFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));
         
         $this->add(array(
             'name' => 'intrests-fieldset',
             'type' => 'User\Form\Edit\IntrestsFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));
         
         
         $this->addUploadElement();
         $this->addInputFilter();
         

         $this->add(array(
             'type' => 'Zend\Form\Element\Csrf',
             'name' => 'csrf',
         ));
         
         $this->add(array(
             'type' => 'submit',
             'name' => 'submit',
             'attributes' => array(
                 'value' => 'Confirm',
                 'class' => 'confirm'
             )
         ));
     }
     
     public function addUploadElement()
    {
        // File Input
        $file = new Element\File('image-file');
        $file->setLabel('Avatar Image Upload')
             ->setAttributes(array(
                 'id' => 'image-file',
                 'accept' => 'image/*',
                 'multiple' => true
             ));
        $this->add($file);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        // File Input
        $fileInput = new InputFilter\FileInput('image-file');
        $fileInput->setRequired(true);

        // You only need to define validators and filters
        // as if only one file was being uploaded. All files
        // will be run through the same validators and filters
        // automatically.
        $fileInput->getValidatorChain()
            ->attachByName('filesize',      array('max' => 90000000))
            ->attachByName('filemimetype',  array('mimeType' => 'image/jpeg,image/png,image/x-png,image/jpg,image/gif'))
            ->attachByName('fileimagesize', array('maxWidth' => 6000, 'maxHeight' => 6000));

        // All files will be renamed, i.e.:
        //   ./data/tmpuploads/avatar_4b3403665fea6.png,
        //   ./data/tmpuploads/avatar_5c45147660fb7.png
        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            array(
                'target'    => './public/uploads/images/profile.png',
                'randomize' => true,
            )
        );
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
    }
 }

?>