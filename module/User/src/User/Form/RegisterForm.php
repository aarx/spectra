<?php

 namespace User\Form;

 use Zend\Form\Form;

 class RegisterForm extends Form
 {
     public function __construct($name = null, $options=array())
     {
         
         parent::__construct($name, $options);
         
         $this->add(array(
             'name' => 'user-fieldset',
             'type' => 'User\Form\RegisterFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));

         $this->add(array(
             'type' => 'submit',
             'name' => 'submit',
             'attributes' => array(
                 'value' => 'Register',
                 'class' => 'user-input button-user'
             )
         ));
     }
 }

?>