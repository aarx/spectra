<?php

namespace User\Form;

use User\Model\User;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

class LoginFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new User());

      $this->add(array(
         'type' => 'text',
         'name' => 'username',
         'attributes'=>array(
            'size'=> '100',
            'required' => true,
             'class' => 'login-form userName',
             'placeholder' => 'Insert your username',
             'autocomplete' => 'off',
             
        )
      ));

      $this->add(array(
         'type' => 'password',
         'name' => 'password',
         'attributes'=>array(
            'size'=> '100',
            'required' => true,
             'class' => 'login-form password',
             'placeholder' => 'Insert your password',
             'autocomplete' => 'off',
        )
      ));
       
      $this->add(array(
         'type' => 'Zend\Form\Element\Checkbox',
         'name' => 'rememberme',
         'attributes'=>array(
            'use_hidden_element' => true,
            'checked_value' => 'good',
            'unchecked_value' => 'bad'
            ),
          'options' => array(
            'label' => 'Remeber me',
          ),
      ));
   }
}

?>