<?php

namespace User\Form;

use User\Model\User;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

class RegisterFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new User());

      $this->add(array(
         'type' => 'hidden',
         'name' => 'userid'
      ));

      $this->add(array(
         'type' => 'text',
         'name' => 'username',
          'attributes' => array(
            'required' => true,
              'class'=>'register-form',
              'placeholder'=>'Insert your username',
              'autocomplete' => 'off',
              
          )
         
      ));
       
       $this->add(array(
         'type' => 'email',
         'name' => 'email',
         'attributes' => array(
            'required' => true,
             'class'=>'register-form',
             'placeholder'=>'Insert your email address',
             'autocomplete' => 'off',
          )
         
      ));
       
       $this->add(array(
         'type' => 'password',
         'name' => 'password',
         'attributes' => array(
            'required' => true,
             'class'=>'register-form',
             'placeholder'=>'Insert your password',
             'autocomplete' => 'off',
          )
         
      ));
       
       
       $this->add(array(
         'type' => 'password',
         'name' => 'passwordConfirm',
         'attributes' => array(
            'required' => true,
             'class'=>'register-form',
             'placeholder'=>'Confirm your password',
             'autocomplete' => 'off',
          )
         
      ));
       
       
        
   }
}

?>