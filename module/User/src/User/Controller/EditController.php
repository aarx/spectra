<?php

 namespace User\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\View\Model\JsonModel;
 use Zend\Form\FormInterface;
 use Zend\Session\Container;
 use Zend\Authentication\AuthenticationService;

 use User\Form\EditForm;
 use User\Model\Edit;
 use User\Service\EditServiceInterface;

 use DateTime;

class EditController extends AbstractActionController 
{
    /**
      * @var \User\Service\EditServiceInterface
      */
     protected $editService;
    
    /**
      * Session variable
      * @var \User\Controller\EditController
      */
     protected $userDetails;
    
    
    public function __construct(EditServiceInterface $editService)
     {
         $this->editService = $editService;
        
         $this->userDetails = new Container('userDetails');
    }
    
    public function indexAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $userId = $this->userDetails->userId;
            if($request->getPost('image-file')) {
                
                $result = $this->uploadFormAction(); 
            }
            try {
                //\Zend\Debug\Debug::dump(new Edit($userId, $request->getPost()));die();
                if(empty($request->getPost())) {
                    return array();
                }
                $result = $this->editService->editUser(new Edit($userId, $request->getPost()));

            } catch (\Exception $e) {
                echo $e->getMessage();
            }
            return new JsonModel(array(
                'result' => $result
            ));
        }
       
    }
    
    public function uploadAction()
    {
        if(!$this->userDetails->userId) {
            $this->redirect()->toRoute('home');
        }
        $form     = new EditForm('edit-form');
        
        $tempFile = null;
        //\Zend\Debug\Debug::dump($this->userDetails->userId);die();
        $prg = $this->fileprg($form);
        if ($prg instanceof \Zend\Http\PhpEnvironment\Response) {
            return $prg; // Return PRG redirect response
        } elseif (is_array($prg)) {
            if ($form->isValid()) {
                $data = $form->getData();
                // Form is valid, save the form!
                return $this->redirect()->toRoute('upload-form/success');
            } else {
                // Form not valid, but file uploads might be valid...
                // Get the temporary file information to show the user in the view
                $fileErrors = $form->get('image-file')->getMessages();
                if (empty($fileErrors)) {
                    $tempFile = $form->get('image-file')->getValue();
                    $tempFile['userId'] = $this->userDetails->userId;
                    $uploadResult = $this->editService->uploadImage($tempFile);
                }
            }
        }
        
        //\Zend\Debug\Debug::dump($this->userDetails->userId);die();
        return array(
            'form'     => $form,
            'tempFile' => $uploadResult,
        );
    }


    public function uploadProgressAction()
    {
        $id = $this->params()->fromQuery('id', null);
        $progress = new \Zend\ProgressBar\Upload\SessionProgress();
        return new \Zend\View\Model\JsonModel($progress->getProgress($id));
    }

}

?>