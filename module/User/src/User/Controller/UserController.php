<?php

 namespace User\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\View\Model\JsonModel;
 use Zend\Form\FormInterface;
 use Zend\Session\Container;
 use Zend\Authentication\AuthenticationService;


 use User\Model\User;
 use User\Form\LoginForm;
 use User\Form\RegisterForm;
 use User\Form\EditForm;
 use User\Service\UserServiceInterface;
 use User\Controller\LoginStorage;

 use DateTime;

class UserController extends AbstractActionController 
{
    /**
      * @var \User\Service\UserServiceInterface
      */
     protected $userService;

    /**
      * @var \User\Form\RegisterForm
      */
     protected $registerForm;
     
    /**
      * @var \User\Form\LoginForm
      */
     protected $loginForm;
    
    /**
      * Session variable
      * @var \User\Controller\UserController
      */
     protected $userDetails;
    
    /**
      * Session storage
      * @var \User\Controller\LoginStorage
      */
     protected $rememberme;

    
    public function __construct(
        UserServiceInterface $userService,
        FormInterface $registerForm,
        FormInterface $loginForm)
     {
         $this->userService = $userService;
         $this->registerForm = $registerForm;
         $this->loginForm = $loginForm;
        
         $this->userDetails = new Container('userDetails');
         $this->registerConfirmEmail = new Container('registeredEmail');
         $this->rememberme = new LoginStorage();
    }
    
    public function indexAction()
    {
        
        /*$editForm = $this->forward()->dispatch('User\Controller\Edit', array(
            'action' => 'upload',
        ));*/
        $this->layout()->setVariable('loggedUser', $this->userDetails);
        $request = $this->getRequest();
        if($request->isPost()) {
            if($request->getPost('getUser')) {
                //\Zend\Debug\Debug::dump($request->getPost('getUser'));die();
                return new JsonModel(array(
                    'username' => $this->userDetails->username,
                ));
            }
        }
        //$form = new UploadForm('upload-form');
        if($this->userDetails->userId) {
            //\Zend\Debug\Debug::dump($this->userDetails->userId);die();
            $userPage = $this->params()->fromRoute('user');
            if($this->userDetails->username != $userPage) {
                $user = $this->userService->getUser($userPage);
                //\Zend\Debug\Debug::dump($user);die();
                if(!$user) {
                    //if user not found
                }
                return new ViewModel(array(
                    'loggedUser' => $this->userDetails,
                    'loggedUserDetails' => $user,
                    
                ));
            } else {
                //\Zend\Debug\Debug::dump($editForm);die();
                return array(
                    'loggedUser' => $this->userDetails,
                    'loggedUserDetails' => $this->userService->getUser($this->userDetails->userId),
                    'form' => $editForm,
                    
                );
            }
        } else {
            return $this->redirect()->toRoute('home');
        }
        
    }
    
    
    public function registerAction()
    {   
        if($this->userDetails->username) {
            return $this->redirect()->toRoute('user',
                                         array('user' => $this->userDetails->username)
                                      );
            
        }
        
        $request = $this->getRequest();
        
         if ($request->isPost()) {
            $this->registerForm->setData($request->getPost());
             $password = $request->getPost('user-fieldset')['password'];

            if ($this->registerForm->isValid()) {
                try {
                    //\Zend\Debug\Debug::dump($this->registerForm->getData());die();
                    if(empty($request->getPost())) {
                        return array();
                    }
                    $result = $this->userService->registerUser($this->registerForm->getData());


                } catch (\Exception $e) {
                    echo $e->getMessage();
                    // Some DB Error happened, log it and let the user know
                }
             }
         }
        
         if($result) {
                 $this->userDetails->userId = $result->getId();
                 $this->userDetails->username = $result->getUsername();
                 $this->userDetails->email = $result->getEmail();
                 $this->userDetails->password = $password;
             //\Zend\Debug\Debug::dump($result->getEmail());die();
             return $this->redirect()->toRoute('register/success');
            /*return new ViewModel(array(
                'registeredUser' => $result,
                'success' => true
            ));*/
         }
         return array('form' => $this->registerForm);
    }
    
    public function loginAction()
    {   
        if($this->userDetails->username) {
            return $this->redirect()->toRoute('user',
                                         array('user' => $this->userDetails->username)
                                      );
            
        }
        
        $request = $this->getRequest();
         if ($request->isPost()) {
             if($request->getPost('login-fieldset')['rememberme'] == '1') {
                $this->rememberme->setRememberMe(1);
             } else {
                $this->rememberme->setRememberMe();
             }
            $this->loginForm->setData($request->getPost());
            
            if ($this->loginForm->isValid()) {
                try {
                    //\Zend\Debug\Debug::dump($this->registerForm->getData());die();
                    if(empty($request->getPost())) {
                        return array();
                    }
                    $result = $this->userService->loginUser($this->loginForm->getData());


                } catch (\Exception $e) {
                    echo $e->getMessage();
                    // Some DB Error happened, log it and let the user know
                }
             }
             if(gettype($result) != 'string') {
                 $this->userDetails->userId = $result->getId();
                 $this->userDetails->username = $result->getUsername();
                 $this->userDetails->email = $result->getEmail();
                 $this->userDetails->loginTime = new DateTime();
                 
                 //\Zend\Debug\Debug::dump($userDetails->username);die();
                 
                 return $this->redirect()->toRoute('user', 
                                                   array('user' => $this->userDetails->username)
                                                  );
             } else {
                return array(
                    'form' => $this->loginForm,
                    'error' => $result
                );
             }
         }
         return array('form' => $this->loginForm);
    }
    
    public function editAction()
    {
    }
    
    public function deleteAction()
    {   
    }
    
    public function logoutAction()
    {
        if($this->userDetails) {
            $this->userDetails->getManager()->destroy();
            return $this->redirect()->toRoute('login');
        }
    }
    
    public function successAction()
    {
        $subject = 'Spectra - Registration confirmation';
        $message = '<h2>Dear '.$this->userDetails->username.',</h2> <p>Your account has been successfully registered to  Spectra community!</p> '
            .'<li>Username: '.$this->userDetails->username.'</li><li>Password: '.$this->userDetails->password.'</li>'
            .'<li>Email: '.$this->userDetails->email.'</li></br>Have a great day,</br>your Spectra team';
        $to = $this->userDetails->email;
        $headers = 'From: noreply@ambassador.ee' . "\r\n" .
                   'Content-Type: text/html; charset=ISO-8859-1\r\n'.
                   'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
        $view =  new ViewModel(array(
            'registeredEmail' => $this->userDetails->email,
        ));
        if($this->registerConfirmEmail) {
            $this->registerConfirmEmail->getManager()->getStorage()->clear('userDetails');
        }
        return $view;
    }
}

?>