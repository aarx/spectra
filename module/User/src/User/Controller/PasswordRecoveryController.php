<?php

 namespace User\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\Form\FormInterface;
 use Zend\Session\Container;
 use Zend\Authentication\AuthenticationService;


 use User\Model\User;
 use User\Form\LoginForm;
 use User\Form\RegisterForm;
 use User\Service\UserServiceInterface;

 use DateTime;

class PasswordRecoveryController extends AbstractActionController 
{
    /**
      * @var \User\Service\UserServiceInterface
      */
     protected $userService;

    /**
      * @var \User\Form\RegisterForm
      */
     protected $registerForm;
     
    /**
      * @var \User\Form\LoginForm
      */
     protected $loginForm;
    
    /**
      * Session variable
      * @var \User\Controller\UserController
      */
     protected $userDetails;
    
    /**
      * Registration confirmation email
      * @var \User\Controller\UserController
      */
     protected $registerConfirmEmail;
    
    public function __construct(
        UserServiceInterface $userService,
        FormInterface $registerForm,
        FormInterface $loginForm)
     {
         $this->userService = $userService;
         $this->registerForm = $registerForm;
         $this->loginForm = $loginForm;
        
         $this->userDetails = new Container('userDetails');
         $this->registerConfirmEmail = new Container('registeredEmail');
    }
    
    public function indexAction()
    {
        return new ViewModel(array());
        
    }

    
    public function changeAction()
    {   
        if($this->userDetails->username) {
            return $this->redirect()->toRoute('user',
                                         array('user' => $this->userDetails->username)
                                      );
            
        }
        
        $request = $this->getRequest();
        
         if ($request->isPost()) {
            $this->registerForm->setData($request->getPost());

            if ($this->registerForm->isValid()) {
                try {
                    //\Zend\Debug\Debug::dump($this->registerForm->getData());die();
                    if(empty($request->getPost())) {
                        return array();
                    }
                    $result = $this->userService->registerUser($this->registerForm->getData());


                } catch (\Exception $e) {
                    echo $e->getMessage();
                    // Some DB Error happened, log it and let the user know
                }
             }
         }
        
         if($result) {
             $this->registerConfirmEmail->email = $result->getEmail();
             //\Zend\Debug\Debug::dump($result->getEmail());die();
             return $this->redirect()->toRoute('register/success');
            /*return new ViewModel(array(
                'registeredUser' => $result,
                'success' => true
            ));*/
         }
         return array('form' => $this->registerForm);
    }
    
    
}

?>