<?php
 namespace User\Controller;
 
 use Zend\Authentication\Storage; 
 use Zend\View\Model\ViewModel;
 
 class LoginStorage extends Storage\Session
 {
    public function setRememberMe($rememberMe = 0, $time = 1209600) 
      { 
          if ($rememberMe == 1){ 
             $this->session->getManager()->rememberMe($time); 
         } 
      }
 }

?>