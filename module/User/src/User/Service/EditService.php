<?php

 namespace User\Service;

 use User\Mapper\EditMapperInterface;
 use User\Model\EditInterface;

 class EditService implements EditServiceInterface
 {
     /**
      * @var \Chat\Mapper\UsrMapperInterface
      */
     protected $editMapper;
     
     /**
      * @param UserMapperInterface $userMapper
      */
     public function __construct(EditMapperInterface $editMapper)
     {
         $this->editMapper = $editMapper;
     }
     
     
     /**
      * {@inheritDoc}
      */
     public function editUser(EditInterface $edit)
     {
         return $this->editMapper->edit($edit);
     }
     
     /**
      * {@inheritDoc}
      */
     public function uploadImage($image)
     {
         return $this->editMapper->upload($image);
     }

 }

?>