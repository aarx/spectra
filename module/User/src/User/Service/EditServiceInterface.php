<?php

 namespace User\Service;

 use User\Model\EditInterface;

 interface EditServiceInterface
 {
     /**
      * Should update a given implementation of the EditInterface and return it. 
      *
      * @return array|EditInterface[]
      */
     public function editUser(EditInterface $edit);
     
     /**
      * Should insert uploaded image details and return them. 
      *
      * @return array|
      */
     public function uploadImage($image);

 }

?>