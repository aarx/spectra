<?php

 namespace User\Service;

 use User\Mapper\UserMapperInterface;
 use User\Model\UserInterface;

 class UserService implements UserServiceInterface
 {
     /**
      * @var \Chat\Mapper\UsrMapperInterface
      */
     protected $userMapper;
     
     /**
      * @param UserMapperInterface $userMapper
      */
     public function __construct(UserMapperInterface $userMapper)
     {
         $this->userMapper = $userMapper;
     }
     
     
     /**
      * {@inheritDoc}
      */
     public function findAllMessages()
     {
         return $this->userMapper->findAll();
     }

     /**
      * {@inheritDoc}
      */
     public function find($id)
     {
         return $this->userMapper->find($id);
     }
     
     /**
      * {@inheritDoc}
      */
     public function registerUser(UserInterface $user)
     {
         return $this->userMapper->register($user);
     }
     
     /**
      * {@inheritDoc}
      */
     public function loginUser(UserInterface $user)
     {
         return $this->userMapper->login($user);
     }
     
     /**
      * {@inheritDoc}
      */
     public function deleteUser(UserInterface $user)
     {
         return $this->userMapper->delete($user);
     }
     
     /**
      * {@inheritDoc}
      */
     public function getUser($user)
     {
         return $this->userMapper->get($user);
     }
 }

?>