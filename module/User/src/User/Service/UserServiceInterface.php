<?php

 namespace User\Service;

 use User\Model\UserInterface;

 interface UserServiceInterface
 {
     /**
      * Should return a set of all messages that we can iterate over. Single entries of the array are supposed to be
      * implementing \Chat\Model\MessageInterface
      *
      * @return array|MessageInterface[]
      */
     public function findAllMessages();

     /**
      * Should return a single chat message
      *
      * @param  int $id Identifier of the Message that should be returned
      * @return MessageInterface
      */
     public function find($id);
     
     /**
      * Should save a given implementation of the UserInterface and return it. If it is an existing User the new User
      * should be denied, if it's a new User it should be created.
      *
      * @param  UserInterface $user
      * @return UserInterface
      */
      public function registerUser(UserInterface $user);
     
     /**
      * Should save a given implementation of the UserInterface and return it. If it is an existing User the new User
      * should be denied, if it's a new User it should be created.
      *
      * @param  UserInterface $user
      * @return UserInterface
      */
      public function loginUser(UserInterface $user);
     
     /**
      * Should delete a given implementation of the UserInterface and return true if the deletion has been
      * successful or false if not.
      *
      * @param  UserInterface $user
      * @return bool
      */
     public function deleteUser(UserInterface $user);
     
     /**
      * 
      *
      * @param  $user
      * @return EditInterface
      */
     public function getUser($user);
     
 }

?>