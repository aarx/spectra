<?php

 namespace User\Mapper;

 use User\Model\EditInterface;

 use Zend\Db\Adapter\AdapterInterface;
 use Zend\Db\Adapter\Driver\ResultInterface;
 use Zend\Db\ResultSet\HydratingResultSet;
 use Zend\Db\Sql\Insert;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Delete;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Update;
 use Zend\Stdlib\Hydrator\HydratorInterface;
 use Zend\Db\ResultSet\ResultSet;


 class EditMapper implements EditMapperInterface
 {
     /**
      * @var \Zend\Db\Adapter\AdapterInterface
      */
     protected $dbAdapter;
     
     /**
      * @var \Zend\Stdlib\Hydrator\HydratorInterface
      */
     protected $hydrator;

     /**
      * @var \User\Model\EditInterface
      */
     protected $editPrototype;

     /**
      * @param AdapterInterface  $dbAdapter
      * @param HydratorInterface $hydrator
      * @param EditInterface  $editPrototype
      */
     public function __construct(
         AdapterInterface $dbAdapter,
         HydratorInterface $hydrator,
         EditInterface $editPrototype
     ) {
         $this->dbAdapter      = $dbAdapter;
         $this->hydrator       = $hydrator;
         $this->editPrototype  = $editPrototype;
     }

     
    /**
    * @param EditInterface $userObject
    *
    * @return EditInterface
    * @return array() - error message
    * @throws \Exception
    */
     public function edit(EditInterface $editObject)
     {
         $editData = $this->hydrator->extract($editObject);
         foreach($editData as $key => $value) {
            if(empty($editData[$key])) {
                unset($editData[$key]);
            }
         }
         //\Zend\Debug\Debug::dump($editObject->getUser_id());die();
    
        $sql = new Sql($this->dbAdapter);
        $select = new Select('userDetails');
        $select->where(array(
            'user_id = ?' => $editObject->getUser_id()
        ));
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        
         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             if(count($result)) {
                // ID present, it's an Update
                $action = new Update('userDetails');
                $action->set($editData);
                $action->where(array('user_id = ?' => $editObject->getUser_id()));
            } else {
                // ID NOT present, it's an Insert
                $action = new Insert('userDetails');
                $action->values($editData);
            }
         } 
        
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface){
            return array(
                'userData' => $editData,
                'success' => true, 
                'connection' => 'OK'
            );
         }
         
         throw new \Exception ('Database error');
     }
     
     public function upload($image)
     {
        $values = array();
        /*foreach($image as $key => $value) {
            if(is_array($image[$key])) {
                foreach($image as $subKey => $subValue) {
                    if(!empty($image[$subKey])) {
                        $values[$subKey] = $subValue; 
                    }
                }
            } else {
                if(empty($image[$key])) {
                    unset($image[$key]);
                } else if($key == 'userId') {
                    $tmp_id = $image[$key];
                    unset($image[$key]);
                    $values['uploader_id'] = $tmp_id;
                }
            }
            
         }*/
         
        foreach($image as $key => $value) {
            if(!empty($image[$key])) {
                $values[$key] = $value;
            }
        }
        \Zend\Debug\Debug::dump($values);die();
        $sql = new Sql($this->dbAdapter);
        $select = new Insert('imageUploads');
        $select->values($values);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
         
        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            \Zend\Debug\Debug::dump($result->current());die();
             return $result->current();
        } 
        
        throw new \Exception ('Database error');
     }
     
 }