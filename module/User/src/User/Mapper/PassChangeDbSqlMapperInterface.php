<?php
 
 namespace User\Mapper;

 use User\Model\UserInterface;

 interface PassChangeDbSqlMapperInterface
 {
     
     /**
      * @param UserInterface $userObject
      *
      * @return bool
      * @throws \Exception
      */
     public function changePass(UserInterface $userObject);
     
 }