<?php

 namespace User\Mapper;

 use User\Model\UserInterface;

 use Zend\Db\Adapter\AdapterInterface;
 use Zend\Db\Adapter\Driver\ResultInterface;
 use Zend\Db\ResultSet\HydratingResultSet;
 use Zend\Db\Sql\Insert;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Delete;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Update;
 use Zend\Stdlib\Hydrator\HydratorInterface;
 use Zend\Db\ResultSet\ResultSet;


 class PassChangeDbSqlMapper implements PassChangeDbSqlMapperInterface
 {
     /**
     * @var \User\Controller\UserController
     */
     private $options = array(
        'cost' => 11,
     );
     /**
      * @var \Zend\Db\Adapter\AdapterInterface
      */
     protected $dbAdapter;
     
     /**
      * @var \Zend\Stdlib\Hydrator\HydratorInterface
      */
     protected $hydrator;

     /**
      * @var \User\Model\UserInterface
      */
     protected $userPrototype;

     /**
      * @param AdapterInterface  $dbAdapter
      * @param HydratorInterface $hydrator
      * @param UserInterface  $userPrototype
      */
     public function __construct(
         AdapterInterface $dbAdapter,
         HydratorInterface $hydrator,
         UserInterface $userPrototype
     ) {
         $this->dbAdapter      = $dbAdapter;
         $this->hydrator       = $hydrator;
         $this->userPrototype  = $userPrototype;
     }
     
     /**
      * {@inheritDoc}
      */
     public function changePass(UserInterface $userObject)
     {
         userData = $this->hydrator->extract($userObject);
         
         //\Zend\Debug\Debug::dump($userObject->getUsername());die();
         

         $action = new Select('users');
         $action->where(array(
             'username = ?' => $userObject->getEmail())
             ); 
         
         //\Zend\Debug\Debug::dump($action->getSqlString());die();
         
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);
             
             $resultSet->initialize($result);
             //\Zend\Debug\Debug::dump($resultSet);die();
             foreach($resultSet as $user) {
                 //\Zend\Debug\Debug::dump($user);
                 //\Zend\Debug\Debug::dump($userData['password_hash']);
                 if($user->getUsername() == $userData['username'] && password_verify($password, $user->getPassword_hash())) {
                     //\Zend\Debug\Debug::dump($user->getUsername());
                     return $user;
                 } else if($user->getUsername() == $userData['username'] && !password_verify($password, $user->getPassword_hash())) {
                    return 'Login error. Wrong password!';
                 } 
             }
             return 'Login error. No such account.';
         } 
         
         throw new \Exception ('Database error');
     }
     
 }