<?php

 namespace User\Mapper;

 use User\Model\UserInterface;
 use User\Model\EditInterface;

 use Zend\Db\Adapter\AdapterInterface;
 use Zend\Db\Adapter\Driver\ResultInterface;
 use Zend\Db\ResultSet\HydratingResultSet;
 use Zend\Db\Sql\Insert;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Delete;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Update;
 use Zend\Stdlib\Hydrator\HydratorInterface;
 use Zend\Db\ResultSet\ResultSet;


 class ZendDbSqlMapper implements UserMapperInterface
 {
     /**
     * @var \User\Controller\UserController
     */
     private $options = array(
        'cost' => 11,
     );
     /**
      * @var \Zend\Db\Adapter\AdapterInterface
      */
     protected $dbAdapter;
     
     /**
      * @var \Zend\Stdlib\Hydrator\HydratorInterface
      */
     protected $hydrator;

     /**
      * @var \User\Model\UserInterface
      */
     protected $userPrototype;
     
     /**
      * @var \User\Model\EditInterface
      */
     protected $editPrototype;


     /**
      * @param AdapterInterface  $dbAdapter
      * @param HydratorInterface $hydrator
      * @param UserInterface  $userPrototype
      * @param EditInterface  $editPrototype
      */
     public function __construct(
         AdapterInterface $dbAdapter,
         HydratorInterface $hydrator,
         UserInterface $userPrototype,
         EditInterface $editPrototype
     ) {
         $this->dbAdapter      = $dbAdapter;
         $this->hydrator       = $hydrator;
         $this->userPrototype  = $userPrototype;
         $this->editPrototype  = $editPrototype;
     }

     
    /**
    * @param int $user
    *
    * @return EditInterface
    * @throws \Exception
    */
    public function get($user)
    {
         //\Zend\Debug\Debug::dump($userId);die();
        $sql = new Sql($this->dbAdapter);
        $select = new Select('users');
        $select->where(array(
            'username = ?' => $user
        ));

        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
            $userId = $result->current()['id'];
            //\Zend\Debug\Debug::dump($userId);die();
            $select = new Select('userDetails');
            $select->where(array(
                'user_id = ?' => $userId
            ));
        }
            
        if(intval($user)){
            $select = new Select('userDetails');
                $select->where(array(
                    'user_id = ?' => $user
                ));
        }
        
            $stmt = $sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
       
            if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
                return $this->hydrator->hydrate($result->current(), $this->editPrototype);
                //\Zend\Debug\Debug::dump($resultSet);die();
            } else {
                return $this->hydrator->hydrate(array(), $this->editPrototype);
            }
        
        
        
    }
    /**
    * @param UserInterface $userObject
    *
    * @return UserInterface
    * @throws \Exception
    */
     public function register(UserInterface $userObject)
     {
         $userData = $this->hydrator->extract($userObject);
         unset($userData['id']);
         unset($userData['timestamp']);
         unset($userData['passwordConfirm']);
         unset($userData['passwordHash']);
         $userData['password_hash'] = password_hash($userData['password'], PASSWORD_BCRYPT, $this->options);
         unset($userData['password']);
    
         $action = new Insert('users');
         $action->values($userData);
        
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface){
            return $userObject;
         }
         
         throw new \Exception ('Database error');
     }
     
     /**
    * @param UserInterface $userObject
    *
    * @return UserInterface
    * @throws \Exception
    */
     public function login(UserInterface $userObject)
     {
         $userData = $this->hydrator->extract($userObject);
         unset($userData['id']);
         unset($userData['email']);
         unset($userData['timestamp']);
         
         $password = $userData['password'];
         unset($userData['password']);

         $action = new Select('users');
         $action->where(array(
             'username = ?' => $userObject->getUsername())
             ); 
         
         
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);
             $resultSet->initialize($result);
        
             foreach($resultSet as $user) {
                 if($user->getUsername() == $userData['username'] && password_verify($password, $user->getPassword_hash())) {
                     return $user;
                 } else if($user->getUsername() == $userData['username'] && !password_verify($password, $user->getPassword_hash())) {
                    return 'Login error. Wrong password!';
                 } 
             }
             return 'Login error. No such account.';
         } 
         
         throw new \Exception ('Database error');
     }
     
     /**
      * {@inheritDoc}
      */
     public function changePass(UserInterface $userObject)
     {
         $this->userData = $this->hydrator->extract($userObject);
         
         //\Zend\Debug\Debug::dump($userObject->getUsername());die();
         

         $action = new Select('users');
         $action->where(array(
             'username = ?' => $userObject->getEmail())
             ); 
         
         //\Zend\Debug\Debug::dump($action->getSqlString());die();
         
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             $resultSet = new HydratingResultSet($this->hydrator, $this->userPrototype);
             
             $resultSet->initialize($result);
             //\Zend\Debug\Debug::dump($resultSet);die();
             foreach($resultSet as $user) {
                 //\Zend\Debug\Debug::dump($user);
                 //\Zend\Debug\Debug::dump($userData['password_hash']);
                 if($user->getUsername() == $userData['username'] && password_verify($password, $user->getPassword_hash())) {
                     //\Zend\Debug\Debug::dump($user->getUsername());
                     return $user;
                 } else if($user->getUsername() == $userData['username'] && !password_verify($password, $user->getPassword_hash())) {
                    return 'Login error. Wrong password!';
                 } 
             }
             return 'Login error. No such account.';
         } 
         
         throw new \Exception ('Database error');
     }
     
     /**
      * {@inheritDoc}
      */
     public function delete(UserInterface $userObject)
     {
     }
 }