<?php
 
 namespace User\Mapper;

 use User\Model\EditInterface;

 interface EditMapperInterface
 {
     /**
      * @param EditInterface $userObject
      * @return EditInterface
      * @throws \InvalidArgumentException
      */
     public function edit(EditInterface $editObject);
     
     /**
      * @param array $image
      * @return array
      * @throws \InvalidArgumentException
      */
     public function upload($image);
     
 }