<?php
 
 namespace User\Mapper;

 use User\Model\UserInterface;
 use User\Model\EditInterface;

 interface UserMapperInterface
 {
     
     /**
     * @param int $userId
     *
     * @return UserInterface
     * @throws \Exception
     */
     public function get($user);
     
     /**
      * @param MessageInterface $messageObject
      *
      * @param UserInterface $userObject
      * @return MessageInterface
      * @throws \Exception
      */
     public function register(UserInterface $userObject);
     
     /**
      * @param MessageInterface $messageObject
      *
      * @param UserInterface $userObject
      * @return MessageInterface
      * @throws \Exception
      */
     public function login(UserInterface $userObject);
     
     /**
      * @param UserInterface $userObject
      *
      * @return bool
      * @throws \Exception
      */
     public function delete(UserInterface $userObject);
     
 }