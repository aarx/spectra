<?php

 namespace User\Factory;

 use User\Controller\EditController;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class EditControllerFactory implements FactoryInterface
 {
     /**
      * Create service
      *
      * @param ServiceLocatorInterface $serviceLocator
      *
      * @return mixed
      */
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         $realServiceLocator = $serviceLocator->getServiceLocator();
         $editService        = $realServiceLocator->get('User\Service\EditServiceInterface');

         return new EditController(
             $editService
         );
     }
}

?>