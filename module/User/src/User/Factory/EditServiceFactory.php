<?php
 
 namespace User\Factory;

 use User\Service\EditService;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class EditServiceFactory implements FactoryInterface
 {
     /**
      * Create service
      *
      * @param ServiceLocatorInterface $serviceLocator
      * @return mixed
      */
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         return new EditService(
             $serviceLocator->get('User\Mapper\EditMapperInterface')
         );
     }
 }
?>