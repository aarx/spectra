<?php

 namespace User\Factory;

 use User\Controller\UserController;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class UserControllerFactory implements FactoryInterface
 {
     /**
      * Create service
      *
      * @param ServiceLocatorInterface $serviceLocator
      *
      * @return mixed
      */
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         $realServiceLocator = $serviceLocator->getServiceLocator();
         $userService        = $realServiceLocator->get('User\Service\UserServiceInterface');
         $registerInsertForm = $realServiceLocator->get('FormElementManager')->get('User\Form\RegisterForm');
         $loginInsertForm    = $realServiceLocator->get('FormElementManager')->get('User\Form\LoginForm');

         return new UserController(
             $userService,
             $registerInsertForm,
             $loginInsertForm
         );
     }
}

?>