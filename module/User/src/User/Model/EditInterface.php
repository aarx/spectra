<?php

 namespace User\Model;

 interface EditInterface
 {
    /**
      * Will return the ID of the user
      *
      * @return int
      */
     public function getUser_id();


     /**
      * Will return the FIRSTNAME of the user
      *
      * @return string
      */
     public function getFirst_name();
     
     /**
      * Will return the MIDDLENAME of the user
      *
      * @return string
      */
     public function getMiddle_name();
     
     /**
      * Will return the LASTNAME of the user
      *
      * @return string
      */
     public function getLast_name();
     
     /**
      * Will return the BIRTHDAY of the user
      *
      * @return date
      */
     public function getBirthday();
     
     /**
      * Will return the INTRESTS of the user
      *
      * @return string
      */
     public function getIntrests();
     
     /**
      * Will return the IMAGE_DETAILS of the user
      *
      * @return array
      */
     public function getImage_id();
     
     /**
      * Will return the TIMESTAMP of the edit
      *
      * @return timestamp
      */
     public function getTimestamp();
 }

?>