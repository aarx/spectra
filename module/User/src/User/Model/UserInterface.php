<?php

 namespace User\Model;

 interface UserInterface
 {
    /**
      * Will return the ID of the user
      *
      * @return int
      */
     public function getId();


     /**
      * Will return the TEXT of the user
      *
      * @return string
      */
     public function getUsername();
     
     /**
      * Will return the PASSWORD of the user
      *
      * @return string
      */
     public function getPassword();
     
     /**
      * Will return the PASSWORDHASH of the user
      *
      * @return string
      */
     public function getPasswordHash();
     
     /**
      * Will return the PASSWORDHASH of the user
      *
      * @return string
      */
     public function getPassword_hash();
     
     /**
      * Will return the TIMESTAMP of the user
      *
      * @return date
      */
     public function getTimestamp();
 }

?>