<?php

 namespace User\Model;

 use Zend\Form\Annotation;

 class User implements UserInterface
 {
     /**
      * @var int
      */
     protected $id;

     /**
      * @var string
      */
     protected $username;

     /**
      * @var string
      */
     protected $email;
     
     /**
      * @var string
      */
     protected $password;
     
     /**
      * @var string
      */
     protected $passwordHash;
     
     /**
      *  $passwordHash var for db interactions.
      * @var string
      */
     protected $password_hash;
     
     /**
      * @var string
      */
     protected $timestamp;
     
     /**
     * @Annotation\Type("Zend\Form\Element\Checkbox")
     * @Annotation\Options({"label":"Remember Me ?:"})
     */
    //public $rememberme;

     /**
      * {@inheritDoc}
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param int $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * {@inheritDoc}
      */
     public function getUsername()
     {
         return $this->username;
     }

     /**
      * @param string $username
      */
     public function setUsername($username)
     {
         $this->username = $username;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getEmail()
     {
         return $this->email;
     }

     /**
      * @param string $email
      */
     public function setEmail($email)
     {
         $this->email = $email;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getPassword()
     {
         return $this->password;
     }

     /**
      * @param string $password
      */
     public function setPassword($password)
     {
         $this->password = $password;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getPasswordHash()
     {
         return $this->passwordHash;
     }

     /**
      * @param string $password_hash
      */
     public function setPassword_hash($password_hash)
     {
         $this->password_hash = $password_hash;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getPassword_hash()
     {
         return $this->password_hash;
     }

     /**
      * @param string $passwordHash
      */
     public function setPasswordHash($passwordHash)
     {
         $this->passwordHash = $passwordHash;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getTimestamp()
     {
         return $this->timestamp;
     }

     /**
      * @param string $timestamp
      */
     public function setTimestamp($timestamp)
     {
         $this->timestamp = $timestamp;
     }
 }

?>