<?php

 namespace User\Model;

 class Edit implements EditInterface
 {
     /**
      * @var int
      */
     protected $user_id;

     /**
      * @var string
      */
     protected $first_name;
     
     /**
      * @var string
      */
     protected $middle_name;

     /**
      * @var string
      */
     protected $last_name;
     
     /**
      * @var string
      */
     protected $birthday;
     
     /**
      * @var string
      */
     protected $intrests;
     
     /**
      * @var array
      */
     protected $image_id;
     
     /**
      * @var string
      */
     protected $timestamp;
     

     public function __construct($userId=null, $edit=array())
     {
         $this->setUser_id($userId);
         $this->setFirst_name($edit['firstName']);
         $this->setMiddle_name($edit['middleName']);
         $this->setLast_name($edit['lastName']);
         $this->setBirthday($edit['birthday']);
         $this->setIntrests($edit['intrests']);
         $this->setImage_id($edit['imageId']);
         $this->setTimestamp($edit['timestamp']);
         
     }
     
     /**
      * {@inheritDoc}
      */
     public function getUser_id()
     {
         return $this->user_id;
     }

     /**
      * @param int $userId
      */
     public function setUser_id($userId)
     {
         $this->user_id = $userId;
     }

     /**
      * {@inheritDoc}
      */
     public function getFirst_name()
     {
         return $this->first_name;
     }

     /**
      * @param string $first_name
      */
     public function setFirst_name($firstName)
     {
         $this->first_name = $firstName;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getMiddle_name()
     {
         return $this->middle_name;
     }

     /**
      * @param string $middle_name
      */
     public function setMiddle_name($middleName)
     {
         $this->middle_name = $middleName;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getLast_name()
     {
         return $this->last_name;
     }

     /**
      * @param string $lastName
      */
     public function setLast_name($lastName)
     {
         $this->last_name = $lastName;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getBirthday()
     {
         return $this->birthday;
     }

     /**
      * @param string $birthday
      */
     public function setBirthday($birthday)
     {
         $this->birthday = $birthday;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getIntrests()
     {
         return $this->intrests;
     }

     /**
      * @param string $intrests
      */
     public function setIntrests($intrests)
     {
         $this->intrests = $intrests;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getImage_id()
     {
         return $this->image_id;
     }

     /**
      * @param string $image
      */
     public function setImage_id($imageId)
     {
         $this->image_id = $imageId;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getTimestamp()
     {
         return $this->timestamp;
     }

     /**
      * @param string $timestamp
      */
     public function setTimestamp($timestamp)
     {
         $this->timestamp = $timestamp;
     }
 }

?>