<?php

 namespace Chat\Form;

 use Zend\Form\Form;

 class MessageForm extends Form
 {
     public function __construct($name = null, $options = array())
     {
         parent::__construct($name, $options);
         
        $this->add(array(
             'name' => 'message-fieldset',
             'type' => 'Chat\Form\MessageFieldset',
             'options' => array(
                 'use_as_base_fieldset' => true
             )
         ));

         
     }
 }

?>