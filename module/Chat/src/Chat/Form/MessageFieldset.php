<?php

namespace Chat\Form;

use Chat\Model\Message;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

class MessageFieldset extends Fieldset
{
   public function __construct($name = null, $options = array())
   {
      parent::__construct($name, $options);
       
      $this->setHydrator(new ClassMethods(false));
      $this->setObject(new Message());

      $this->add(array(
         'type' => 'text',
         'name' => 'message',
         'attributes' => array(
            'class' => 'chatInput chatBox textField',
            'placeholder'=>'Say something nice',
            'autocomplete' => 'off',
          )
      ));
       
     $this->add(array(
         'type' => 'submit',
         'name' => 'submit',
         'attributes' => array(
             'value' => 'Send',
             'class' => 'chatInput button-chat',
         )
     ));
   }
}

?>