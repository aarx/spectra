<?php

namespace Chat\Form;

use Zend\Form\Form;


class ChatForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('chat');
        
        $this->add(array(
            'name' => 'userId',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name'=>'userName',
            'type'=>'Text',
            'options'=>array(
                'label'=>'Username',
            ),
        ));
        
        $this->add(array(
            'name'=>'chatText',
            'type'=>'Text',
            'options'=> array(
                'label'=>'Insert text to chat',
            ),
        ));
        
        $this->add(array(
            'name'=>'submit',
            'type'=>'Submit',
            'attributes'=>array(
                'value'=>'Chat',
                'id'=>'submitbutton',     
            ),
        ));    
            
    
    }
}

?>