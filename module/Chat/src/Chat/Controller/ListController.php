<?php

 namespace Chat\Controller;

 use Chat\Service\MessageServiceInterface;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\View\Model\JsonModel;
 use Zend\Form\FormInterface;
 use Zend\Json\Json;
 use Zend\Stdlib\Hydrator;
 use Zend\Stdlib\ArrayUtils;
 use Zend\Session\Container;

 class ListController extends AbstractActionController
 {
     /**
      * @var \Chat\Service\MessageServiceInterface
      */
     protected $messageService;

     protected $messageForm;
     
     /**
      * session variable $userDetails
      */
     private $userDetails;
     
     
     public function __construct(
        MessageServiceInterface $messageService,
        FormInterface $messageForm)
     {
         $this->messageService = $messageService;
         $this->messageForm    = $messageForm;
         
         $this->userDetails = new Container('userDetails');
     }
     
     public function indexAction()
     {
         
        if(!$this->userDetails->username) {
            return $this->redirect()->toRoute('home');
        }
        $this->layout()->setVariable('loggedUser', $this->userDetails);
        
        
        $request = $this->getRequest();
        
         if ($request->isPost()) {
             
             $mode = $request->getPost('mode') ? : 0;
             $lastId = $request->getPost('user2Id') ? : 0;
             
             if(!$mode) {
                $this->messageForm->setData($request->getPost());

                if ($this->messageForm->isValid()) {
                    try {
                        //\Zend\Debug\Debug::dump($this->messageForm->getData());die();
                        if(!empty($request->getPost('message-fieldset[message]'))) {
                            $this->messageService->saveMessage($this->messageForm->getData());
                            $this->messageService->setConversation(array(
                                'user1' => $this->userDetails-userId,
                                'user2' => $request->getPost('user2')
                            ));
                        }
                     
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                    }
                 }
             } else if($mode) {
                  return new JsonModel(array(
                    'messages' => end(\Zend\Stdlib\ArrayUtils::iteratorToArray($this->messageService->findAllMessages())),
                    'loggedUser' => \Zend\Stdlib\ArrayUtils::iteratorToArray($this->userDetails),
                    'success' => true,
                ));
             }
     
         }
         return new ViewModel(array(
            'messages' => $this->messageService->findAllMessages(),
            'loggedUser' => $this->userDetails,
            'form' => $this->messageForm
         ));
         
        
     }
     
     public function detailAction()
     { 
         
         $id = $this->params()->fromRoute('id');

        try {
             $message = $this->messageService->findMessage($id);
         } catch (\InvalidArgumentException $ex) {
             return $this->redirect()->toRoute('messages');
         }

         return new ViewModel(array(
             'message' => $message
         ));
     }
 }

?>