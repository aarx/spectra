<?php

 namespace Chat\Controller;

 use Chat\Service\MessageServiceInterface;

 use Zend\Form\FormInterface;
 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\View\Model\JsonModel;
 use Zend\Json\Json;
 use Zend\Session\Container;

 class WriteController extends AbstractActionController
 {
     protected $messageService;

     protected $messageForm;
     
     /**
      * session variable $userDetails
      */
     private $userDetails;

     public function __construct(
         MessageServiceInterface $messageService,
         FormInterface $messageForm
     ) {
         $this->messageService = $messageService;
         $this->messageForm    = $messageForm;
         
         $this->userDetails = new Container('userDetails');
     }

     public function addAction()
     {
         if(!$this->userDetails->username) {
            return $this->redirect()->toRoute('home');
        }
         
         $request = $this->getRequest();

         if ($request->isPost()) {
             $this->messageForm->setData($request->getPost());

             if ($this->messageForm->isValid()) {
                 try {
                     //\Zend\Debug\Debug::dump($this->messageForm->getData());die();
                     $this->messageService->saveMessage($this->messageForm->getData());
                 } catch (\Exception $e) {
                     echo $e->getMessage();
                 }
             }
         }
         
         return new ViewModel(array(
             'form' => $this->messageForm,
         ));
     }
     
     public function editAction()
     {
         if(!$this->userDetails->username) {
            return $this->redirect()->toRoute('home');
        }
         
         $request = $this->getRequest();
         $message = $this->messageService->findMessage($this->params('id'));
         
         if($message->getUserId() != $this->userDetails->userId) {
                return $this->redirect()->toRoute('messages');
             }
         
         $this->messageForm->bind($message);

         if ($request->isPost()) {
             $this->messageForm->setData($request->getPost());

             if ($this->messageForm->isValid()) {
                 try {
                     $this->messageService->saveMessage($message);

                     return $this->redirect()->toRoute('messages');
                 } catch (\Exception $e) {
                     die($e->getMessage());
                 }
             }
         }
         
          return new ViewModel(array(
             'form' => $this->messageForm
         ));
    }
 }

?>