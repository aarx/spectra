<?php

 namespace Chat\Controller;

 use Chat\Service\MessageServiceInterface;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\Session\Container;

 class DeleteController extends AbstractActionController
 {
     /**
      * @var \Chat\Service\PostServiceInterface
      */
     protected $messageService;
     
     /**
      * session variable $userDetails
      */
     private $userDetails;

     public function __construct(MessageServiceInterface $messageService)
     {
         $this->messageService = $messageService;
         $this->userDetails = new Container('userDetails');
     }

     public function deleteAction()
     {
         if(!$this->userDetails->username) {
            return $this->redirect()->toRoute('home');
        }
         
         try {
             $message = $this->messageService->findMessage($this->params('id'));
             if($message->getUserId() != $this->userDetails->userId) {
                return $this->redirect()->toRoute('messages');
             }
         } catch (\InvalidArgumentException $e) {
             return $this->redirect()->toRoute('chat');
         }

         $request = $this->getRequest();

         if ($request->isPost()) {
             $del = $request->getPost('delete_confirmation', 'no');

             if ($del === 'yes') {
                 $this->messageService->deleteMessage($message);
             }

             return $this->redirect()->toRoute('messages');
         }

         return new ViewModel(array(
             'message' => $message
         ));
     }
 }