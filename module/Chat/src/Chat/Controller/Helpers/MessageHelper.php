<?php

 namespace Chat\Controller\Helpers;

 use Zend\View\Helper\AbstractHelper;

 use DateTime;

 class MessageHelper extends AbstractHelper
 {
     
    /*
    * param $timestamp
    * @return $parsedDate
    **/
    public static function parseMessageDate($timestamp)
    {
        $currentDate = new DateTime('now');  
        $messageDate = new DateTime($timestamp);
        
        $dateDifference = $currentDate->diff($messageDate);
    
        if($dateDifference->format('%d') == 0) {
            if($dateDifference->format('%i') < 1 && $dateDifference->format('%h') < 1) {
                $parsedDate = 'Just now';
            } else if($dateDifference->format('%i') < 60 && $dateDifference->format('%h') < 1) {
                $parsedDate = $dateDifference->format('%i') . ' minutes ago';
            } else if($dateDifference->format('%h') > 0 && $dateDifference->format('%h') < 24) {
                $parsedDate = $dateDifference->format('%h') . ' hours ago';
            }
        
        } else if($dateDifference->format('%d') > 0 && $dateDifference->format('%d') < 7) {
            $parsedDate = $messageDate->format('l, H:m');
        } else if($dateDifference->format('%d') > 6) {
            $parsedDate = $messageDate->format('d. F');
        }
        return $parsedDate;
    }   
    
    /*
    * param $input
    * @return linkString
    **/
    public static function getLink($input)
    {
        $regex = "/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        return preg_replace_callback($regex, function ($matches) {
            trim($matches[0]);
            return "<a href=\"http://{$matches[0]}\" target=\"_blank\" >{$matches[0]}</a>";
        }, $input);
    }
     
    /*
    * param &$subject
    * @return $subject
    **/
    public static function Smilify(&$subject)
    {
        $smilies = array(
            ':|'  => 'mellow',
            ':-|' => 'mellow',
            ':-o' => 'ohmy',
            ':-O' => 'ohmy',
            ':o'  => 'ohmy',
            ':O'  => 'ohmy',
            ';)'  => 'wink',
            ';-)' => 'wink',
            ':p'  => 'tongue',
            ':-p' => 'tongue',
            ':P'  => 'tongue',
            ':-P' => 'tongue',
            ':D'  => 'biggrin',
            ':-D' => 'biggrin',
            '8)'  => 'cool',
            '8-)' => 'cool',
            ':)'  => 'smile',
            ':-)' => 'smile',
            ':('  => 'sad',
            ':-(' => 'sad',
            ':S' => 'troubled',
            'monsterr' => 'monster',
            ':@' => 'angry',
            '-_-' => '-_-',
            '(A)' => 'angel',
            '(a)' => 'angel',
            'EST' => 'EST',
            'swastika' => 'swastika',
        );

        $sizes = array(
            'biggrin' => 30,
            'cool' => 15,
            'haha' => 20,
            'mellow' => 20,
            'ohmy' => 20,
            'sad' => 20,
            'smile' => 18,
            'tongue' => 30,
            'wink' => 30,
            'swastika' => 50,
        );

        $replace = array();
        foreach ($smilies as $smiley => $imgName)
        {
            $size = $sizes[$imgName];
            array_push($replace, '<img src="../uploads/smileys/'.$imgName.'.gif" alt="'.$smiley.'" width="'.$size.'" height="'.$size.'" />');
        }
        $subject = str_replace(array_keys($smilies), $replace, $subject); 
        return $subject;
    }

 }

?>