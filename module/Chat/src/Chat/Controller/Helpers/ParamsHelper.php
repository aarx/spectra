<?php

 namespace Chat\Controller\Helpers;

 use Chat\Controller\WriteController;
 use Zend\View\Helper\AbstractHelper;

 class ParamsHelper extends AbstractHelper
 {
    public $params;

    public function __construct()
    {
        $request = WriteController::getInstance()->getRequest();
        $this->params = $request->getParams();
    }

    public function myParams()
    {       
        if(isset($this->params['username']))
            unset($this->params['username']);
        return $this->params;
    }

    public function direct()
    {
        return $this->myParams();
    }
 }

?>