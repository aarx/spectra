<?php

 namespace Chat\Mapper;

 use Chat\Model\MessageInterface;
 use Chat\Model\ConversationInterface;

 use Zend\Db\Adapter\AdapterInterface;
 use Zend\Db\Adapter\Driver\ResultInterface;
 use Zend\Db\ResultSet\HydratingResultSet;
 use Zend\Db\Sql\Insert;
 use Zend\Db\Sql\Delete;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Update;
 use Zend\Stdlib\Hydrator\HydratorInterface;
 use Zend\Db\ResultSet\ResultSet;
 use Zend\Session\Container;

 use Zend\Db\Sql\Ddl;
 use Zend\Db\Sql\Ddl\Column;
 use Zend\Db\Sql\Ddl\Constraint;


 class ZendDbSqlMapper implements MessageMapperInterface
 {
     /**
      * @var \Zend\Db\Adapter\AdapterInterface
      */
     protected $dbAdapter;
     
     /**
      * @var \Zend\Stdlib\Hydrator\HydratorInterface
      */
     protected $hydrator;

     /**
      * @var \Chat\Model\MessageInterface
      */
     protected $messagePrototype;
     
     /**
      * @var \Chat\Model\MessageInterface
      */
     protected $convoPrototype;

     /**
      * @param AdapterInterface  $dbAdapter
      * @param HydratorInterface $hydrator
      * @param MessageInterface  $messagePrototype
      * @param ConversationInterface  $convoPrototype
      */
     public function __construct(
         AdapterInterface $dbAdapter,
         HydratorInterface $hydrator,
         MessageInterface $messagePrototype,
         ConversationInterface $convoPrototype
     ) {
         $this->dbAdapter      = $dbAdapter;
         $this->hydrator       = $hydrator;
         $this->messagePrototype  = $messagePrototype;
         $this->convoPrototype  = $convoPrototype;
     }

     /**
      * @param int|string $id
      *
      * @return MessageInterface
      * @throws \InvalidArgumentException
      */
     public function find($id)
     {
         $sql    = new Sql($this->dbAdapter);
         $select = $sql->select('messages');
         $select->join('users', 'messages.userId = users.id', array('username'), 'left');
         $select->where(array('messages.id = ?' => $id));
         

         $stmt   = $sql->prepareStatementForSqlObject($select);
         $result = $stmt->execute();

         if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
             return $this->hydrator->hydrate($result->current(), $this->messagePrototype);
         }

         throw new \InvalidArgumentException("Message with given ID:{$id} not found.");
     }

     /**
      * @return array|MessageInterface[]
      */
     public function findAll()
     {
         $sql    = new Sql($this->dbAdapter);
         $select = $sql->select('messages');
         $select->join('users', 'messages.userId = users.id', array('username'), 'left');
         $select->order(array(
                        'messages.timestamp ASC'
         ));

         $stmt   = $sql->prepareStatementForSqlObject($select);
         $result = $stmt->execute();
         
         if ($result instanceof ResultInterface && $result->isQueryResult()) {
             $resultSet = new HydratingResultSet($this->hydrator, $this->messagePrototype);
             //\Zend\Debug\Debug::dump($resultSet->initialize($result));die();
             return $resultSet->initialize($result);
         }

         return array();
     }
     
    /**
    * @param MessageInterface $messageObject
    *
    * @return MessageInterface
    * @throws \Exception
    */
     public function save(MessageInterface $messageObject)
     {
         $messageData = $this->hydrator->extract($messageObject);
         unset($messageData['id']);
         unset($messageData['username']);
         unset($messageData['timestamp']);
         $loggedUser = new Container('userDetails');
         $messageData['userId'] = $loggedUser->userId;
         //\Zend\Debug\Debug::dump($messageData);die();
         
         if ($messageObject->getId()){
            // ID present, it's an Update
            $action = new Update('messages');
            $action->set($messageData);
            $action->where(array('id = ?'=> $messageObject->getId()));    
         }
         else {
            // ID NOT present, it's an Insert
            $action = new Insert('messages');
            $action->values($messageData);
         }
        
         $sql = new Sql($this->dbAdapter);
         $stmt = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();
         //echo $sql->getSqlstringForSqlObject($select); die ;
         
         if ($result instanceof ResultInterface){
            if ($newId = $result->getGeneratedValue()){
                // When a value has been generated, set it on the object
                $messageObject->setId($newId);
                
            }
            return $messageObject;
         }
         
         throw new \Exception ('Database error');
     }
     
     /**
      * {@inheritDoc}
      */
     public function delete(MessageInterface $messageObject)
     {
         $action = new Delete('messages');
         $action->where(array('id = ?' => $messageObject->getId()));

         $sql    = new Sql($this->dbAdapter);
         $stmt   = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();

         return (bool)$result->getAffectedRows();
     }
     
     /**
      * {@inheritDoc}
      */
     public function create($users)
     {
         $action = new Select('conversations');
         $action->where(array('user_id1 = ?' => $users['user1'], 
                       'user_id2 = ?' => $users['user2']));
         $sql    = new Sql($this->dbAdapter);
         $stmt   = $sql->prepareStatementForSqlObject($action);
         $result = $stmt->execute();
         
         if($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()){
            return $this->hydrator->hydrate($result->current(), $this->convoPrototype);
         } else {
            $action = new Insert('conversations');
            $action->values(array('title' => $users['user1'].' and '.$users['user2'],
                                  'user_id1' => $users['user1'],
                                  'user_id2' => $users['user2']
                                 ));
            $sql    = new Sql($this->dbAdapter);
            $stmt   = $sql->prepareStatementForSqlObject($action);
            $result = $stmt->execute();
             
            $sql    = new Sql($this->dbAdapter);
            $table = new Ddl\CreateTable('conversation_'. $users['user1'] .'-'. $users['user1']);
            $table->addColumn(new Column\Integer('message_id'));
            $table->addColumn(new Column\Text('message', 500));
            $table->addColumn(new Column\Timestamp('timestamp'));
            $table->addColumn(new Column\Integer('user_id'));
             
            $table->addConstraint(new Constraint\PrimaryKey('message_id'));
            $table->addConstraint(new Constraint\ForeignKey('fk_user_id', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'));
             
            $this->dbAdapter->query(
                $sql->getSqlStringForSqlObject($table),
                Adapter::QUERY_MODE_EXECUTE
            ); 
         }

         throw new \Exception ('Database error');
     }
 }