<?php
 
 namespace Chat\Mapper;

 use Chat\Model\MessageInterface;

 interface MessageMapperInterface
 {
     /**
      * @param int|string $id
      * @return MessageInterface
      * @throws \InvalidArgumentException
      */
     public function find($id);

     /**
      * @return array|MessageInterface[]
      */
     public function findAll();
     
     /**
      * @param MessageInterface $messageObject
      *
      * @param MessageInterface $messageObject
      * @return MessageInterface
      * @throws \Exception
      */
     public function save(MessageInterface $messageObject);
     
     /**
      * @param MessageInterface $messageObject
      *
      * @return bool
      * @throws \Exception
      */
     public function delete(MessageInterface $messageObject);
     
     /**
      * @param $users array
      *
      * @return bool
      * @throws \Exception
      */
     public function create($users);
     
 }