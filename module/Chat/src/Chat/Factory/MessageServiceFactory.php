<?php
 
 namespace Chat\Factory;

 use Chat\Service\MessageService;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class MessageServiceFactory implements FactoryInterface
 {
     /**
      * Create service
      *
      * @param ServiceLocatorInterface $serviceLocator
      * @return mixed
      */
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         return new MessageService(
             $serviceLocator->get('Chat\Mapper\MessageMapperInterface')
         );
     }
 }