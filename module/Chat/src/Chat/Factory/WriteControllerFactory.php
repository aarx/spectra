<?php

 namespace Chat\Factory;

 use Chat\Controller\WriteController;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class WriteControllerFactory implements FactoryInterface
 {
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         $realServiceLocator = $serviceLocator->getServiceLocator();
         $messageService        = $realServiceLocator->get('Chat\Service\MessageServiceInterface');
         $messageInsertForm     = $realServiceLocator->get('FormElementManager')->get('Chat\Form\MessageForm');

         return new WriteController(
             $messageService,
             $messageInsertForm
         );
     }
 }

?>