<?php

 namespace Chat\Factory;

 use Chat\Controller\ListController;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class ListControllerFactory implements FactoryInterface
 {
     /**
      * Create service
      *
      * @param ServiceLocatorInterface $serviceLocator
      *
      * @return mixed
      */
     public function createService(ServiceLocatorInterface $serviceLocator)
     {
         $realServiceLocator = $serviceLocator->getServiceLocator();
         $messageService        = $realServiceLocator->get('Chat\Service\MessageServiceInterface');
         $messageInsertForm     = $realServiceLocator->get('FormElementManager')->get('Chat\Form\MessageForm');

         return new ListController(
             $messageService,
             $messageInsertForm
            );
        }
     }

?>