<?php

 namespace Chat\Model;

 class Message implements MessageInterface
 {
     /**
      * @var int
      */
     protected $id;

     /**
      * @var string
      */
     protected $username;

     /**
      * @var int
      */
     protected $userId;
     
     /**
      * @var string
      */
     protected $message;
     
     /**
      * @var string
      */
     protected $timestamp;

     /**
      * {@inheritDoc}
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param int $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * {@inheritDoc}
      */
     public function getUsername()
     {
         return $this->username;
     }

     /**
      * @param string $username
      */
     public function setUsername($username)
     {
         $this->username = $username;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getUserId()
     {
         return $this->userId;
     }

     /**
      * @param string $userId
      */
     public function setUserId($userId)
     {
         $this->userId = $userId;
     }

     /**
      * {@inheritDoc}
      */
     public function getMessage()
     {
         return $this->message;
     }

     /**
      * @param string $text
      */
     public function setMessage($message)
     {
         $this->message = $message;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getTimestamp()
     {
         return $this->timestamp;
     }

     /**
      * @param string $timestamp
      */
     public function setTimestamp($timestamp)
     {
         $this->timestamp = $timestamp;
     }
 }

?>