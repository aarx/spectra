<?php

 namespace Chat\Model;

 class Conversation implements ConversationInterface
 {
     /**
      * @var int
      */
     protected $id;

     /**
      * @var string
      */
     protected $title;

     /**
      * @var int
      */
     protected $user_id1;
     
     /**
      * @var string
      */
     protected $user_id2;
     
     /**
      * @var string
      */
     protected $timestamp;
     
    /**
      * {@inheritDoc}
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param int $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * {@inheritDoc}
      */
     public function getTitle()
     {
         return $this->title;
     }

     /**
      * @param string $title
      */
     public function setUsername($title)
     {
         $this->title = $title;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getUser_id1()
     {
         return $this->user_id1;
     }

     /**
      * @param string $userId
      */
     public function setUser_id1($userId1)
     {
         $this->user_id1 = $userId1;
     }

     /**
      * {@inheritDoc}
      */
     public function getUser_id2()
     {
         return $this->user_id2;
     }

     /**
      * @param string $text
      */
     public function setUser_id2($userId2)
     {
         $this->user_id2 = $userId2;
     }
     
     /**
      * {@inheritDoc}
      */
     public function getTimestamp()
     {
         return $this->timestamp;
     }

     /**
      * @param string $timestamp
      */
     public function setTimestamp($timestamp)
     {
         $this->timestamp = $timestamp;
     }
 }