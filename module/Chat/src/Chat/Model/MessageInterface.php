<?php

 namespace Chat\Model;

 interface MessageInterface
 {
    /**
      * Will return the ID of the message
      *
      * @return int
      */
     public function getId();

     /**
      * Will return the AUTHOR of the message
      *
      * @return string
      */
     public function getMessage();

     /**
      * Will return the TEXT of the message
      *
      * @return string
      */
     public function getUsername();
     
     /**
      * Will return the USER_ID of the message
      *
      * @return int
      */
     public function getUserId();
     
     /**
      * Will return the TIMESTAMP of the message
      *
      * @return date
      */
     public function getTimestamp();
 }

?>