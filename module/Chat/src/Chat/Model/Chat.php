<?php
    
namespace Chat\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputfilterInterface;


class Chat implements InputFilterAwareInterface
{
    public $userId;
    public $userName;
    public $chatText;
    protected $inputFilter;
    
    
    public function exchangeArray($data)
    {
        $this->userId = (isset($data['userId'])) ? $data['userId']:null;
        $this->userName = (isset($data['userName'])) ? $data['userName']:null;
        $this->chatText = (isset($data['chatText'])) ? $data['chatText']:null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter){
        throw new \Exception("Not used");
        
    }
    
    public function getInputFilter()
    {
        if(!this->inputFilter){
            $inputFilter = new InputFilter();
            
            $inputFilter->add(array(
                'name'=>'userId',
                'required'=> true,
                'filters'=>array(
                    array('name'=>'Int'),  
                ),
            ));
            
            $inputFilter->add(array(
                'name'=>'userName',
                'required'=> true,
                'filters'=> array(
                    array('name'=>'StripTags'),
                    array(9
                )
            ))
        }
    }
}





?>