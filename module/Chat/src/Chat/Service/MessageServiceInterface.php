<?php

 namespace Chat\Service;

 use Chat\Model\MessageInterface;

 interface MessageServiceInterface
 {
     /**
      * Should return a conversation title. 
      * Implements \Chat\Model\MessageInterface.
      *
      * @return array|MessageInterface[]
      */
     //public function getConversationTitle();
     
     /**
      * Should return a set of all messages that we can iterate over. Single entries of the array are supposed to be
      * implementing \Chat\Model\MessageInterface
      *
      * @return array|MessageInterface[]
      */
     public function findAllMessages();

     /**
      * Should return a single chat message
      *
      * @param  int $id Identifier of the Message that should be returned
      * @return MessageInterface
      */
     public function findMessage($id);
     
     /**
      * Should save a given implementation of the MessageInterface and return it. If it is an existing Message the Message
      * should be updated, if it's a new Message it should be created.
      *
      * @param  MessageInterface $chat
      * @return MessageInterface
      */
      public function saveMessage(MessageInterface $chat);
     
     /**
      * Should delete a given implementation of the PostInterface and return true if the deletion has been
      * successful or false if not.
      *
      * @param  PostInterface $blog
      * @return bool
      */
     public function deleteMessage(MessageInterface $message);
     
      /**
      * 
      *
      * @param  $users array()
      * @return bool
      */
     public function setConversation($users);
 }

?>