<?php

 namespace Chat\Service;

 use Chat\Mapper\MessageMapperInterface;
 use Chat\Model\MessageInterface;

 class MessageService implements MessageServiceInterface
 {
     /**
      * @var \Chat\Mapper\MessageMapperInterface
      */
     protected $messageMapper;
     
     /**
      * @param MessageMapperInterface $messageMapper
      */
     public function __construct(MessageMapperInterface $messageMapper)
     {
         $this->messageMapper = $messageMapper;
     }
     
     /**
      * {@inheritDoc}
      */
     /*public function getConversationTitle()
     {
         return $this->messageMapper->getTitle();
     }*/
     
     /**
      * {@inheritDoc}
      */
     public function findAllMessages()
     {
         return $this->messageMapper->findAll();
     }

     /**
      * {@inheritDoc}
      */
     public function findMessage($id)
     {
         return $this->messageMapper->find($id);
     }
     
     /**
      * {@inheritDoc}
      */
     public function saveMessage(MessageInterface $message)
     {
         return $this->messageMapper->save($message);
     }
     
     /**
      * {@inheritDoc}
      */
     public function deleteMessage(MessageInterface $message)
     {
         return $this->messageMapper->delete($message);
     }
     
     /**
      * {@inheritDoc}
      */
     public function setConversation($users=array())
     {
         return $this->messageMapper->create($users);
     }
 }

?>