<?php

    return array(
     'service_manager' => array(
        'factories' => array(
            'Chat\Mapper\MessageMapperInterface' => 'Chat\Factory\ZendDbSqlMapperFactory',
            'Chat\Service\MessageServiceInterface' => 'Chat\Factory\MessageServiceFactory',
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory'
        )
     ),
        
     'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view', 
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
     ),
        
     'controllers' => array(
         'factories' => array(
            'Chat\Controller\List' => 'Chat\Factory\ListControllerFactory',
            'Chat\Controller\Write' => 'Chat\Factory\WriteControllerFactory',
             'Chat\Controller\Delete' => 'Chat\Factory\DeleteControllerFactory'
         )
     ),
     // This lines opens the configuration for the RouteManager
     'router' => array(
         // Open configuration for all possible routes
         'routes' => array(
             // Define a new route called "message"
             'messages' => array(
                 // Define the routes type to be "Zend\Mvc\Router\Http\Literal", which is basically just a string
                 'type' => 'literal',
                 // Configure the route itself
                 'options' => array(
                     // Listen to "/messages" as uri
                     'route'    => '/messages',
                     // Define default controller and action to be called when this route is matched
                     'defaults' => array(
                         'controller' => 'Chat\Controller\List',
                         'action'     => 'index',
                     )
                 ),
                 'may_terminate' => true,
                 'child_routes'  => array(
                     'conversation' => array(
                         'type' => 'segment',
                         'options' => array(
                             'route'    => '/conversation[-:id]',
                             'defaults' => array(
                                 'action' => 'detail',
                             ),
                             'constraints' => array(
                                 'id' => '[1-9]\d*'
                             )
                         )
                     ),
                     'detail' => array(
                         'type' => 'segment',
                         'options' => array(
                             'route'    => '/:id',
                             'defaults' => array(
                                 'action' => 'detail',
                             ),
                             'constraints' => array(
                                 'id' => '[1-9]\d*'
                             )
                         )
                     ),
                     'add'=>array(
                        'type'=>'literal',
                        'options'=>array(
                            'route'=>'/add',
                            'defaults'=>array(
                                'controller'=>'Chat\Controller\Write',
                                'action'=>'add'
                            )
                        )
                    ),
                    'edit' => array(
                         'type' => 'segment',
                         'options' => array(
                             'route'    => '/edit/:id',
                             'defaults' => array(
                                 'controller' => 'Chat\Controller\Write',
                                 'action'     => 'edit'
                             ),
                             'constraints' => array(
                                 'id' => '\d+'
                             )
                         )
                     ),
                     'delete' => array(
                         'type' => 'segment',
                         'options' => array(
                             'route'    => '/delete/:id',
                             'defaults' => array(
                                 'controller' => 'Chat\Controller\Delete',
                                 'action'     => 'delete'
                             ),
                             'constraints' => array(
                                 'id' => '\d+'
                             )
                         )
                     ),
                 )
             )  
         )
     )
 );

?>