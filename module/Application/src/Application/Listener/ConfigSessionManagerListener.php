<?php
 namespace Application\Listener;


 class ConfigSessionManagerListener extends AbstractListenerAggregate
 {

    /**
    * {@inheritDoc}
    */
    public function attach(Events $events)
    {
        // very low priority at this level
        $this->listeners[] = $events->attach(ModuleEvent::EVENT_LOAD_MODULES_POST, array($this, 'configSessionManager'), -1000);
    }

    public function configSessionManager(ModuleEvent $e)
    {
        // init session with our config
        $config = $e->getConfigListener()->getMergedConfig(false);
        $sessionConfigOptions = isset($config['session']) ? $config['session'] : array();

        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($sessionConfigOptions);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        Session::setDefaultManager($sessionManager);
    }
 }
?>