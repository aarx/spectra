<?php

 namespace Image\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Zend\Session\Container;

class ImageController extends AbstractActionController 
{
    protected $userDetails;
    
    public function __construct()
    {
        $this->userDetails = new Container('userDetails');
    }
    
    public function indexAction()
    {
        $this->layout()->setVariable('loggedUser', $this->userDetails);
    }
    
    public function addAction()
    {
    }
    
    public function editAction()
    {
    }
    
    public function deleteAction()
    {
    }
}

?>